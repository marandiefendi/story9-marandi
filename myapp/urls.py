from django.urls import path
from . import views
from django.contrib.auth.views import LoginView,LogoutView

app_name = 'myapp'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('dashboard/', views.dashboard, name = 'dashboard_url'),
    path('login/', LoginView.as_view(), name='login_url'),
    path('register/', views.register, name='register_url'),
    path('logout/', LogoutView.as_view(next_page= '/'), name='logout_url')
]