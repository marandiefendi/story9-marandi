from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class Story9_Test(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index_html_exist(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_url_not_exist(self):
        response = Client().get('/oi')
        self.assertEqual(response.status_code, 404)

    def test_login_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200) 
    
    def test_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_url_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 302)

    def test_register_post_using_register_template(self):
        response = Client().post('/register/', {'username':'marandime'})
        self.assertTemplateUsed(response, 'registration/register.html')

class TestWithSelenium(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)

    def test_register_selenium(self):
        self.driver.get("http://localhost:8000")
        self.driver.find_element_by_id('text-nav-oauth1').click()
        self.driver.find_element_by_id('id_username').send_keys("kakasdosuwu123")
        self.driver.find_element_by_id('id_password1').send_keys("inipassworduwu123")
        self.driver.find_element_by_id('id_password2').send_keys("inipassworduwu123")
        self.driver.find_element_by_id('tombolRegis').click()
        
    def tearDown(self):
        self.driver.quit



